// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  ambiente: 'Ambiente de Pruebas',
  apiUrl: 'http://10.30.3.227:8080/sicofam/api',
  page: {
    maxResultPerPage: 10,
    sizeOptions: [10, 25, 100]
  },
  snackbar: {
    duration: 2500
  },
  valorEstatusEdicion : 'EDICION',
  areaSelected: 2,
  catAreas : {
    FAM: {
      id: 1,
      name: 'FAM'
    },
    contabilidad: {
      id: 2,
      name: 'Contabilidad',
      icon: 'calculate'
    },
    cuentasPagar: {
      id: 3,
      name: 'Cuentas por Pagar',
      icon: 'attach_money'
    },
    sistematizacion: {
      id: 4,
      name: 'Sistematización',
      icon: 'computer'
    },
    recursosHumanos: {
      id: 5,
      name: 'Recursos Humanos',
      icon: ''
    },
    administrador: {
      id: 6,
      name: 'Administrador',
      icon: ''
    }
  },
  minioBucket: 'sicofam-qa',
  estatusCFDIS : {
    sistematizacion: {
      cancelado: {
        name: 'CANCELADO',
        color: '#c9c9c9'
      },
      validado: {
        name: 'TIMBRADO',
        color: 'rgba(45, 202, 95, 1)'
      },
      retimbrado: {
        name: 'RETIMBRADO',
        color: 'rgba(45, 202, 95, 1)'
      }
    },
    FAM : {
      validado: {
        name: 'VALIDADO',
        color: 'rgba(45, 202, 95, 1)'
      },
      rechazado: {
        name: 'NO_VALIDADO',
        color: '#f3b428'
      },
      pendiente: {
        name: 'PENDIENTE_DE_VALIDAR',
        color: '#000'
      }
    }
  },
  envioCorreos: true
};
