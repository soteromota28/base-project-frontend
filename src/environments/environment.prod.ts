export const environment = {
  production: true,
  ambiente: 'Producción',
  apiUrl: 'http://localhost:8080/sicofam/api',
  page: {
    maxResultPerPage: 10,
    sizeOptions: [10, 25, 100]
  },
  snackbar: {
    duration: 3000
  },
  valorEstatusEdicion : 'EDICION',
  areaSelected: 2,
  catAreas : {
    FAM: {
      id: 1,
      name: 'FAM'
    },
    contabilidad: {
      id: 2,
      name: 'Contabilidad',
      icon: 'calculate'
    },
    cuentasPagar: {
      id: 3,
      name: 'Cuentas por Pagar',
      icon: 'attach_money'
    },
    sistematizacion: {
      id: 4,
      name: 'Sistematización',
      icon: 'computer'
    },
    recursosHumanos: {
      id: 5,
      name: 'Recursos Humanos',
      icon: ''
    },
    administrador: {
      id: 6,
      name: 'Administrador',
      icon: ''
    }
  },
  minioBucket: 'sicofam-prod',
  estatusCFDIS : {
    sistematizacion: {
      cancelado: {
        name: 'CANCELADO',
        color: '#c9c9c9'
      },
      validado: {
        name: 'TIMBRADO',
        color: 'rgba(45, 202, 95, 1)'
      },
      retimbrado: {
        name: 'RETIMBRADO',
        color: 'rgba(45, 202, 95, 1)'
      }
    },
    FAM : {
      validado: {
        name: 'VALIDADO',
        color: 'rgba(45, 202, 95, 1)'
      },
      rechazado: {
        name: 'NO_VALIDADO',
        color: '#f3b428'
      },
      pendiente: {
        name: 'PENDIENTE_DE_VALIDAR',
        color: '#000'
      }
    }
  },
  envioCorreos: true
};
