import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { ChartistModule } from 'ng-chartist';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../shared/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { GraficasModule } from '../shared/components/graficas/graficas.module';
import { FlexLayoutModule } from '@angular/flex-layout';


const COMPONENTES = [
  DashboardComponent,
];

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(DashboardRoutes),
    SharedModule,
    GraficasModule,
    AngularMaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [COMPONENTES],
  exports: [COMPONENTES]
})
export class DashboardModule {}
