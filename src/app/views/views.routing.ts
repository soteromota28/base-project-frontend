import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [

  {
    path: '',
    redirectTo: 'ejemplo',
    pathMatch: 'full'
  },
  {
    path: '',
    data: {
      breadcrumb: 'Páginas'
    },
    children: [

     /*  {
        path: 'catalogos-list',
        data: {
          breadcrumb: 'Catálogos'
        },
        loadChildren: () => import('./catalogos/catalogos.module').then(m => m.CatalogosModule)
      } */
  ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule {}

