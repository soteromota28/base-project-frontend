import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DemoMaterialModule } from '../demo-material-module';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ViewsRoutingModule } from './views.routing';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    SharedModule,
    ViewsRoutingModule,
    RouterModule
  ],
  providers: [],
  entryComponents: [],
  declarations: [],
  exports: []
})
export class ViewsModule {}
