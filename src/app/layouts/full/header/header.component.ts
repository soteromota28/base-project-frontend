import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../authentication/services/auth.service';
import { UserI } from '../../../authentication/models/user';
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: []
})
export class AppHeaderComponent implements OnInit {
  nameUser;
  nameArea;
  catAreasFront = environment.catAreas;
  mostrarConfiguraciones: boolean;
  constructor(
    private _auth: AuthService
  ) {

  }

  ngOnInit() {
    this.nameUser = 'Usuario de Pruebas';
    this.nameArea = 'ADMINISTRADOR';
  }

  logout(){
    this._auth.logout();
  }

  

}

