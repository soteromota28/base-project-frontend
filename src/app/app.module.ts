import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';

import { FullComponent } from './layouts/full/full.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './demo-material-module';

import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';
import { FooterComponent } from './layouts/full/footer/footer.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatPaginatorIntl } from '@angular/material';
import { CustomPaginator } from './custom-paginator';
import { LoginComponent } from './authentication/login/login.component';
import { BreadcrumbModule } from 'xng-breadcrumb';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthGuardService } from './authentication/guard/auth.guard.service';
import { AuthService } from './authentication/services/auth.service';
import { UserI } from './authentication/models/user';
import { JwtModule } from '@auth0/angular-jwt';
import { LoadingService } from './shared/services/loading.service';
import { LoadingInterceptor } from './shared/services/loading.interceptor';


export function tokenGetterFn() {
  const user: UserI = JSON.parse(localStorage.getItem('user'));
  return user ? user.token : null;
}

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    AppHeaderComponent,
    SpinnerComponent,
    AppSidebarComponent,
    FooterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    AppRoutingModule,
    BreadcrumbModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetterFn
      }
    })
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}
    },
    { provide: MatPaginatorIntl, useValue: CustomPaginator() },
    AuthService,
    AuthGuardService,
    LoadingService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  exports: [SharedModule, FlexLayoutModule]
})
export class AppModule {}
