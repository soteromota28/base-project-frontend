import { Routes, RouterModule } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { NgModule } from '@angular/core';
import { LoginComponent } from './authentication/login/login.component';
import { AuthGuardService } from './authentication/guard/auth.guard.service';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: FullComponent,
    //canActivate: [AuthGuardService],
    data: {
      breadcrumb: {
        label: 'Inicio',
        info: 'home',
      },
    },
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
      },
      {
        path: '',
        loadChildren: () => import('./views/views.module').then(m => m.ViewsModule),
      },
      {
        path: 'dashboard',
        data: {
          breadcrumb: {
            label: 'Dashboard',
            info: 'dashboard',
          },
        },
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
      },

    ],
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
