import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CredentialI } from '../models/credential';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs';
import { environment } from "../../../environments/environment";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  dataUser: CredentialI;
  loginReference: Subscription = null;
  areas: any;
  ambiente = environment.ambiente;

  imgLoginFondo = '../assets/images/greca.png';
  imgLoginCabecera = '../assets/images/logos/sesver.png';

  constructor(
    private formBuilder: FormBuilder,
    private _auth: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }


  buildForm() {
    this.form = this.formBuilder.group({
      user: ['', Validators.required],
      pass: ['', Validators.required]
    });
  }

  login() {
    this.dataUser = {
      'usuario'  : this.form.controls.user.value,
      'password' : this.form.controls.pass.value,
      'app'      : 'SICOFAM'
    };


    this.router.navigate(['dashboard']);
    /* this.loginReference = this._auth.login(this.dataUser); */

  }

  ngOnDestroy(): void {
    //this.loginReference.unsubscribe();
  }

}
