export interface UserI {
    name: string;
    token: string;
    data: any;
    area?: any;
}
