import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';
import { AuthenticationRoutes } from './authentication.routing';
import { ChartistModule } from 'ng-chartist';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from '../layouts/full/footer/footer.component';


@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(AuthenticationRoutes)
  ],
  declarations: [LoginComponent, FooterComponent],
  providers: []
})
export class AuthenticationModule {}