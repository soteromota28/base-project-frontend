import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private _authService: AuthService,
        private router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot){
        if (this._authService.authenticated) {
            return true;
        }
        this.router.navigate(['login']);
        return false;
    }
}
