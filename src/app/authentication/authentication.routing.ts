import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';

export const AuthenticationRoutes: Routes = [
  {

    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    data: {
      title: 'Autenticación'
    },
    children: [
      {
        path: 'login',
        loadChildren: () => import('./login/login.component').then(m => m.LoginComponent)
      },
    ]
  }
];
