import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CredentialI } from '../models/credential';
import { UserI } from '../models/user';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  protected url = environment.apiUrl;
  protected header = new HttpHeaders();
  user: UserI;

  constructor(
    private jwtHelper: JwtHelperService,
    protected httpClient: HttpClient,
    private router: Router,
  ) {

  }

  public get authenticated(): boolean {
    const token = this.jwtHelper.tokenGetter();
    if (token) {
      return !this.jwtHelper.isTokenExpired(token);
    }
    return false;

  }

  public login(data: CredentialI) {
    this.router.navigate(['dashboard']);
  }

  public currentUser(): UserI {
    return this.user;
  }

  public logout() {
    this.router.navigate(['login']);
  }


  


}
