import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NombreUsuarioPipe } from './nombre-usuario.pipe';
import { QuincenaPipe } from './quincena.pipe';

const PIPES = [
  NombreUsuarioPipe,
  QuincenaPipe
];

@NgModule({
  declarations: [PIPES],
  exports: [PIPES],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
