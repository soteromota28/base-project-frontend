import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nombreUsuario'
})
export class NombreUsuarioPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    return value.Nombre + ' ' + value.Paterno + ' ' + value.Materno;
  }

}
