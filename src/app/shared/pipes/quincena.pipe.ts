import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'quincena'
})
export class QuincenaPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    return value.numero + ' (' + value.diaInicio + ' - ' + value.diaFin + ' ' + value.mes + ')';
  }

}
