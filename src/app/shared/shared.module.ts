import { NgModule } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionDirective, AccordionLinkDirective } from './accordion';
import { InputComponent } from './components/input/input.component';
import { InputNumberComponent } from './components/input-number/input-number.component';
import { InputDecimalComponent } from './components/input-decimal/input-decimal.component';
import { SelectComponent } from './components/select/select.component';
import { AngularMaterialModule } from './angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule, PercentPipe } from '@angular/common';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { ToggleComponent } from './components/toggle/toggle.component';
import { TableComponent } from './components/table/table.component';
import { ProgressSpinnerComponent } from './components/progress-spinner/progress-spinner.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { RouterModule } from '@angular/router';
import { CardComponent } from './components/card/card.component';
import { ButtonComponent } from './components/button/button.component';
import { TextAreaComponent } from './components/text-area/text-area.component';
import { RadioComponent } from './components/radio/radio.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { DragDropFileUploadComponent } from './components/drag-drop-file-upload/drag-drop-file-upload.component';
import { SearchComponent } from './components/search/search.component';
import { DecimalesDirective } from './directives/decimales.directive';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CurrencyPipe } from '@angular/common';
import { InputPasswordComponent } from './components/input-password/input-password.component';
import { PipesModule } from './pipes/pipes.module';



const COMPONENTS = [
  InputComponent,
  SelectComponent,
  DatepickerComponent,
  ToggleComponent,
  TableComponent,
  ProgressSpinnerComponent,
  BreadcrumbComponent,
  CardComponent,
  ButtonComponent,
  TextAreaComponent,
  RadioComponent,
  CheckboxComponent,
  DragDropFileUploadComponent,
  SearchComponent,
  InputNumberComponent,
  InputDecimalComponent,
  InputPasswordComponent
];


@NgModule({
  imports: [
    AngularMaterialModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    CurrencyMaskModule,
    PipesModule
  ],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    COMPONENTS,
    DecimalesDirective
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    COMPONENTS,
    DecimalesDirective,
    PipesModule
   ],
  providers: [
    MenuItems,
    CurrencyPipe,
    PercentPipe
  ]
})
export class SharedModule { }
