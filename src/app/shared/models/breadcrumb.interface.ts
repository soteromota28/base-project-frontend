export interface BreadCrumbI {
  label: string;
  url: string;
}
