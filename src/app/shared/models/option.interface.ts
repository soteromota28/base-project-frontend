export interface OptionI {
  label: string;
  value: any;
  disabled?: boolean;
  selected?: boolean;
}
