export interface ColumnTableI {
  field: string;
  title: string;
  cell?: any;
  verData?: any;
  width?: any;
  style?:any;
}
