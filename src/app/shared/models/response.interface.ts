export interface ResponseI<E> {
  mensaje: string;
  codigo: string;
  data: E;
}
