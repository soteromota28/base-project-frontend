/* tslint:disable */

/**
 * Entidad que devuelve la paginación de una entidad
 */
export interface Page {

    /**
     * Total de elementos encontrados con los filtros establecidos
     */
    totalElements?: number;
  
    /**
     * Listado con los registros paginados
     */
    content?: any[];
  }
  