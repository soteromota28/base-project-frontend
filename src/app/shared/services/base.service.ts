import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ResponseI } from '../models/response.interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BaseService {

  protected url = environment.apiUrl;
  // protected header = new HttpHeaders().set('Authorization', `Bearer ${JSON.parse(localStorage.getItem('user'))['token']}`);

  constructor(
    protected httpClient: HttpClient) {
  }

  public post(endPoint: string, data: any): Observable<ResponseI<any>> {
    return this.httpClient
      .post<ResponseI<any>>(`${this.url}/${endPoint}`, data)
      .pipe(map(response => <ResponseI<any>>response));
  }

  public put(endPoint: string, data: any): Observable<ResponseI<any>> {
    return this.httpClient
      .put<ResponseI<any>>(`${this.url}/${endPoint}`, data)
      .pipe(map(response => <ResponseI<any>>response));
  }

  public get(endPoint: string, params?: any): Observable<ResponseI<any>> {
    return this.httpClient
      .get<ResponseI<any>>(`${this.url}/${endPoint}`,
        {params: params})
      .pipe(map(response => <ResponseI<any>>response));
  }

  public delete(endPoint: string): Observable<ResponseI<any>> {
    return this.httpClient
      .delete<ResponseI<any>>(`${this.url}/${endPoint}`)
      .pipe(map(response => <ResponseI<any>>response));
  }

  public download(endPoint: string, params?: any): Observable<Blob> {
    return this.httpClient
      .get(`${this.url}/${endPoint}`, { params: params, responseType: 'blob'});
  }

  public getImagenes(endPoint: string): Observable<ResponseI<any>> {
    return this.httpClient
      .get<ResponseI<any>>(`${this.url}/${endPoint}`)
      .pipe(map(response => <ResponseI<any>>response));
  }
}
