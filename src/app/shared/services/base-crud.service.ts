import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseI } from '../models/response.interface';
import { BaseService } from './base.service';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BaseCrudService extends BaseService {

  constructor(
    protected httpClient: HttpClient,
    @Inject('ENDPOINT') protected endpoint: string
  ) {
    super(httpClient);
  }

  public save(data: any): Observable<ResponseI<any>> {
    return this.post(this.endpoint, data);
  }

  public update(data: any): Observable<ResponseI<any>> {
    return this.put(this.endpoint, data);
  }

  public page(params?: any): Observable<ResponseI<any>> {
    return this.get(this.endpoint, params);
  }

  public show(id: any): Observable<ResponseI<any>> {
    return this.get(`${this.endpoint}/${id}`);
  }

  public list(): Observable<ResponseI<any>> {
    return this.get(`${this.endpoint}/list`);
  }

  public deleteRecord(id: any): Observable<ResponseI<any>> {
    return this.delete(`${this.endpoint}/${id}`);
  }

}
