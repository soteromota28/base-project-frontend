import { FormGroup } from '@angular/forms';
import { BaseCrudService } from '../services/base-crud.service';
import { PageEvent } from '@angular/material';
import { Page } from '../models/page';
import { ColumnTableI } from '../models/column-table.interface';
import { environment } from '../../../environments/environment';
import Swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs';

export abstract class BaseCrud {

  form: FormGroup;
  data: any;
  dataBuscador = '';
  anioActual;

  /* usuario de la session*/
  user: any;

  /* catalogo de las areas desde el environment */
  catAreasFront = environment.catAreas;

  /* estatus cfdis */
  estatusCFDIS = environment.estatusCFDIS;

  limpiar = new BehaviorSubject(false);

  // Mensajes de respuesta
  saveMsg = 'Registro guardado con éxito';
  updateMsg = 'Registro actualizado con éxito';
  deleteMsg = 'Registro eliminado con éxito';
  deleteAlert = 'Esta seguro de eliminar el resgistro';
  // ------------------------
  // Notificaciones
  // ------------------------
  // Paginador
  pageData: Page[] = [];
  pageParams: any;
  maxResultPerPage = environment.page.maxResultPerPage;
  totalRowsPage = 0;
  pageColumns: ColumnTableI[] = [];

  // ------------------------


  protected constructor(
    public baseCrudService: BaseCrudService
  ) {

    this.user = JSON.parse(localStorage.getItem('user'));

    this.pageParams = {
      maxResults: this.maxResultPerPage,
      firstResult: 0,
    };

    let today = new Date();
    this.anioActual = today.getFullYear();

  }

  save = (form: any): void => {
    this.baseCrudService.save(form).subscribe(
      response => {
        this.data = response['data'];
        this.form.reset();
        this.afterSave();
        this.showAlert('success', this.saveMsg);
      },
      error => {
        console.error(error);
        this.showAlert('error', error);
      },
    );
  };

  update = (data: any) => {
    this.baseCrudService.update(data).subscribe(
      response => {
        this.afterUpdate();
        this.showAlert('success', this.updateMsg);
      },
      error => {
        console.error(error);
        this.showAlert('error', error);
      },
    );
  };

  page = (): void => {
    this.baseCrudService.page(this.pageParams).subscribe(
      response => {
        this.pageData = response['data']['content'];
        this.totalRowsPage = response['data']['totalElements'];
        this.afterPage();
      },
      error => {
        console.error(error);
      },
    );
  };

  show = (id: any): void => {
    this.baseCrudService.show(id).subscribe(
      response => {
        this.data = response['data'];
        this.afterShow();
      },
      error => {
        console.error(error);
      },
    );
  };

  afterShow = () => {
    // Implementar acciones después de mostrar el registro
  };

  afterSave = () => {
    // Implementar acciones despues de guardar el registro
  };

  afterDelete = () => {

  };

  afterUpdate = () => {

  };

  afterPage = () => {

  };

  list = (): void => {
    this.baseCrudService.list().subscribe(
      response => {
      },
      error => {
        console.error(error);
      },
    );
  };

  delete = (row: any): void => {
 Swal.fire({
      title: '<html><body><strong style="color: #e9ab2e">Alerta</strong></body></html>',
      text: '¿Está seguro que desea eliminar el registro?',
      icon: 'warning',
      showCancelButton: true,
      buttonsStyling: true,
      focusConfirm: false,
      cancelButtonColor: '#E8E8E8',
      cancelButtonText: '<html><body><span style="color: #2f353a">Cancelar</span></body></html>',
      confirmButtonColor: '#d33',
      confirmButtonText: '<html><body><span style="color: #FFFFFF; border-color: #FFFFFF;">Eliminar</span></body></html>',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.baseCrudService.deleteRecord(row.id).subscribe(
          response => {
            this.afterDelete();
            this.showAlert('success', this.deleteMsg);
          },
          error => {
            console.error(error);
            this.showAlert('error', error);
          },
        );
      }
    });
  }

  changePage = (event: PageEvent) => {
    this.pageParams = {
      maxResults: event.pageSize,
      firstResult: event.pageIndex * event.pageSize,
    };
    this.page();
  };

  showAlert(icon, data) {
    let title = '';
    let timer = null;
    switch (icon) {
      case 'error':
        title = 'Error ' + data.error?.codigo;
        timer = null;
        data = data.error?.mensaje;
        break;
      case 'success':
        title = 'Correcto';
        timer = 2000;
        break;
    }
    Swal.fire({
      title: title,
      text: data,
      icon: icon,
      confirmButtonText: 'Aceptar',
      timer: timer,
    });
  }


  showAlertHTML(icon, data) {
    let title = 'Correcto';
    let timer = null;
   
    Swal.fire({
      title: title,
      html: data,
      icon: icon,
      confirmButtonText: 'Aceptar',
      timer: timer,
    });
  }

  buscar(e) {
    this.dataBuscador = e.trim();
    this.pageParams = {
      maxResults: environment.page.maxResultPerPage,
      firstResult: 0,
      filtro: this.dataBuscador,
    };
    this.page();
  }

  alertDonwload() {
    this.showAlert('success', 'Documento Descargado!');
  }

  removerItemArray = ( arr, item ) => {
    return arr.filter( e => e !== item );
  }

}
