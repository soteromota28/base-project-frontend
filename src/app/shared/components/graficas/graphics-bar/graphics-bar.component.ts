import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { Color } from 'ng2-charts/public_api';


@Component({
  selector: 'app-graphics-bar',
  templateUrl: './graphics-bar.component.html',
  styleUrls: ['./graphics-bar.component.css']
})

export class GraphicsBarComponent implements OnInit {

  barChartLabels: Label[];
  barChartData: ChartDataSets[];
  @Input() dataGrafica: any;
  @Input() barChartLegend = true;

  titulo: string;

  barChartType: ChartType = 'bar';
  //public barChartLegend = true;
  barChartPlugins = [pluginDataLabels];
  public barChartColors: Color[] = [
    { backgroundColor: 'rgba(153,86,86,0.3)' },
    { backgroundColor: 'rgba(132,170,94,0.3)' },
    { backgroundColor: 'rgba(0,0,255,0.2)' },
  ]

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  constructor() { }

  ngOnInit() {
    if (this.dataGrafica) {
      this.barChartData = this.dataGrafica.data;
      this.barChartLabels = this.dataGrafica.labels;
      this.titulo = this.dataGrafica.titulo;
    }
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


}
