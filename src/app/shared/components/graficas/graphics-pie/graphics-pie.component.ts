import { Component, Input, OnInit } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-graphics-pie',
  templateUrl: './graphics-pie.component.html',
  styleUrls: ['./graphics-pie.component.css']
})
export class GraphicsPieComponent implements OnInit {

  pieChartLabels: Label[];
  pieChartData: number[];
  @Input() dataGrafica: any;
  @Input() barChartLegend = true;
  titulo: string;

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };


  public pieChartType: ChartType = 'pie';
  public pieChartPlugins = [pluginDataLabels];
  public pieChartLegend = true;
  public pieChartColors = [
    {
    backgroundColor: ['rgba(76,130,134,0.3)', 'rgba(217,223,134,0.3)', 'rgba(217,142,84,0.3)', 'rgba(0, 160, 84,0.3)', 'rgba(0,0,255,0.2)', 'rgba(0,20,100,0.2)'],
    },
  ];

  constructor() { }

  ngOnInit() {
    if (this.dataGrafica) {
      this.pieChartData = this.dataGrafica.data;
      this.pieChartLabels = this.dataGrafica.labels;
      this.titulo = this.dataGrafica.titulo;
    }
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
