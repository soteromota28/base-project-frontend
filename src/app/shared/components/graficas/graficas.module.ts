import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphicsPieComponent } from './graphics-pie/graphics-pie.component';
import { GraphicsBarComponent } from './graphics-bar/graphics-bar.component';
import { GraphicsDynamicComponent } from './graphics-dynamic/graphics-dynamic.component';
import { ChartsModule } from 'ng2-charts';


const GRAFICAS = [
  GraphicsBarComponent,
  GraphicsPieComponent,
  GraphicsDynamicComponent
];

@NgModule({
  declarations: [
    GRAFICAS
  ],
  imports: [
    CommonModule,
    ChartsModule
  ],
  exports: [
    GRAFICAS
  ]
})
export class GraficasModule { }
