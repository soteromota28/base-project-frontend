import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicsDynamicComponent } from './graphics-dynamic.component';

describe('GraphicsDynamicComponent', () => {
  let component: GraphicsDynamicComponent;
  let fixture: ComponentFixture<GraphicsDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicsDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicsDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
