import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { Color } from 'ng2-charts/public_api';

@Component({
  selector: 'app-graphics-dynamic',
  templateUrl: './graphics-dynamic.component.html',
  styleUrls: ['./graphics-dynamic.component.css']
})
export class GraphicsDynamicComponent implements OnInit {

  barChartLabels: Label[];
  barChartData: ChartDataSets[];
  @Input() dataGrafica: any;
  @Input() barChartLegend = true;
  titulo: string;

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
  };


  public barChartType: ChartType = 'line';
  public lineChartColors: Color[] = [
    { backgroundColor: 'rgba(153,86,86,0.3)' },
    { backgroundColor: 'rgba(132,170,94,0.3)' },
    { backgroundColor: 'rgba(0,0,255,0.2)' },
  ]


  constructor() { }

  ngOnInit() {
    if (this.dataGrafica) {
      this.barChartData = this.dataGrafica.data;
      this.barChartLabels = this.dataGrafica.labels;
      this.titulo = this.dataGrafica.titulo;
    }
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


}
