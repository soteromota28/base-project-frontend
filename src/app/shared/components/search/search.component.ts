import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { BaseComponent } from '../base/base-component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent extends BaseComponent implements OnInit {

  @Input() limpiar: BehaviorSubject<boolean>;
  @Output() dataBuscador = new EventEmitter<string>();

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) { 
    super();
  }

  ngOnInit(): void {
    this.builderForm();
    this.limpiar.next(false);


    this.limpiar.subscribe((data) => {
      if(data){
        this.limpiarFormulario();
      }
    });

  }

  builderForm() {
    this.form = this.formBuilder.group({
      buscador: [''],
    });

  }

  buscar(){
    this.dataBuscador.emit(this.form.value.buscador.trim());
  }

  limpiarFormulario(){
    this.form.reset();
  }

}
