import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';

@Component({
  selector: 'app-input-password',
  templateUrl: './input-password.component.html'
})
export class InputPasswordComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  @Input() maxCharacters: number;

  ngOnInit(): void {

  }

}
