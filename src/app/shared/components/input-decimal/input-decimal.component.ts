import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';

@Component({
  selector: 'app-input-decimal',
  templateUrl: './input-decimal.component.html'
})
export class InputDecimalComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  @Input() maxCharacters: number;

  ngOnInit(): void {

  }

}
