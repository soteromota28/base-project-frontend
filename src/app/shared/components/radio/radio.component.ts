import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';
import { RadioOptionI } from '../../models/radio-option.interface';

@Component({
  selector: 'app-radio',
  templateUrl: 'radio.component.html',
  styleUrls: ['./radio.component.scss']
})

export class RadioComponent extends BaseComponent implements OnInit {

  @Input() radioOptions: RadioOptionI[] = [];

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
