import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { ColumnTableI } from '../../models/column-table.interface';
import { PageEvent, MatPaginator, MatTableDataSource, MatTable } from '@angular/material';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-table',
  templateUrl: 'table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit{
  /* @ViewChild('table') table: MatTable<any>; */

  @ViewChild(MatPaginator) paginador: MatPaginator;

  @ContentChild(TemplateRef, { static: true }) _acciones;

  @Input() columns: ColumnTableI[] = [];
  @Input() dataSource = [];
  
  //dataSourceTable = new MatTableDataSource<any>([]);


  @Input() pageSize: number;
  @Input() pageSizeOptions: number[] = environment.page.sizeOptions;
  @Input() length: number;
  @Input() styles: any;
  @Output() changePageEvent = new EventEmitter<PageEvent>();


  /* private _dataSource;
  get dataSource(): any {
    return this._dataSource;
  }

  @Input()
  set dataSource(val: any) {
    this._dataSource = val;
  } */

  constructor() {
  }

  ngOnInit() {

  }

  getDisplayedColumns = () => {
    return this.columns.map(column => column.field);
  }

  changePage = (event: PageEvent) => {
    return this.changePageEvent.emit(event);
  }

  primeraPagina(){
    this.paginador.firstPage();
  }

}
