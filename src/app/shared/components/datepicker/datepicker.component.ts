import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '../base/base-component';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class DatepickerComponent extends BaseComponent implements OnInit {

  today = new Date();
  anioActual = this.today.getFullYear();
  mesActual = this.today.getMonth();
  diaAcutual = this.today.getDay();

  fechaActual = new Date(this.anioActual, this.mesActual, this.diaAcutual);



  @Input() startDate = this.fechaActual;
  @Input() tipoCalendario = 'month';

  constructor() {
    super();
  }

  ngOnInit(): void {

    console.log(this.fechaActual);

  }

}
