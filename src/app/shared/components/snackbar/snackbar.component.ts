import { Component, Inject, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styles: ['.message {width:100%;}']
})

export class SnackbarComponent extends BaseComponent implements OnInit {

  constructor(
    public snackBarRef: MatSnackBarRef<SnackbarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any,
  ) {
    super();
  }

  ngOnInit() {
  }

}
