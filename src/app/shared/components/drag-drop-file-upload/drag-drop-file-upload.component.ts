import { Component, Input, OnInit, ViewChild, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

interface Extensions {
  image?: boolean;
  pdf?: boolean;
  pdfXml?: boolean;
  excel?: boolean;
  word?: boolean;
  jasper?: boolean;
  xml?: boolean;
  pdfImage?: boolean;
}

@Component({
  selector: 'app-drag-drop-file-upload',
  templateUrl: 'drag-drop-file-upload.component.html',
  styleUrls: ['drag-drop-file-upload.scss'],
})
export class DragDropFileUploadComponent implements OnInit, OnChanges {

  files: File[] = [];
  fileErrors: string[] = [];
  dataSource = new MatTableDataSource<File>([]);
  @Input() formGroup: FormGroup;
  @Input() name: string;
  @Input() multiple = true;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('fileInput') fileInput: HTMLInputElement;
  @Input() messageRequired = 'Campo obligatorio';
  @Input() allowExtensions: Extensions;
  @Input() accept: any;
  @Input() renameSingleFile: string;
  @Input() limpiar = false;

  displayedColumns = ['name', 'size', 'acciones'];
  regexImage = /\.(gif|jpe?g|tiff|png|webp|bmp)$/i;
  regexPdf = /\.(pdf)$/i;
  regexExcel = /\.(xlsx|xls|csv)$/i;
  regexJasper = /\.(jasper)$/i;
  regexXml = /\.(xml)$/i;
  regexPdfXml = /\.(xml|pdf)$/i;
  regexPdfImage = /\.(pdf|jpe?g|png)$/i;

  constructor(

  ) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.limpiar) {
      this.limpiarComponente();
    }
  }


  limpiarComponente () {
    this.files = [];
    this.refreshData();
    this.formGroup.reset();
    this.dataSource.data = this.files;
  }

  uploadFile(event) {
    if (!this.multiple) {
      this.files = [];
    }
    for (let index = 0; index < event.length; index++) {
      let element = event[index];
      if (this.checkExtension(element.name)) {
        if (this.renameSingleFile) {
          const filename = this.renameSingleFile + '.' + element.name.split('.').pop();
          element = new File([element], filename, {type: element.type});
        }
        this.files.push(element);
      } else {
        this.fileErrors.push(element.name);
      }
    }
    this.refreshData();
  }

  deleteAttachment = (index: number) => {
    this.files.splice(index, 1);
    this.refreshData();
  };

  deleteAttachments = () => {
    this.files = [];
    this.refreshData();
  };

  readableBytes = (bytes: number) => {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) {
      return 'n/a';
    }
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    if (i === 0) {
      return bytes + ' ' + sizes[i];
    }
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
  }

  refreshData = () => {
    if (this.fileErrors.length > 0) {
      Swal.fire({
        title: 'Archivo(s) con extension(es) inválida(s)',
        text:  this.fileErrors.toString(),
        icon: 'error',
        confirmButtonText: 'Aceptar',
        timer: null
      });
    }
    this.fileErrors = [];
    this.dataSource.data = this.files;
    this.formGroup.controls[this.name].setValue(this.files);
  }

  checkExtension = (filename: string): boolean => {
    let allow = true;
    if (this.allowExtensions.pdf === true) {
      allow = this.regexPdf.test(filename);
    }
    else if (this.allowExtensions.image === true) {
      allow = this.regexImage.test(filename);
    }
    else if (this.allowExtensions.excel === true) {
      allow = this.regexExcel.test(filename);
    }
    else if (this.allowExtensions.jasper === true) {
      allow = this.regexJasper.test(filename);
    }
    else if (this.allowExtensions.xml === true) {
      allow = this.regexXml.test(filename);
    }
    else if (this.allowExtensions.pdfXml === true){
      allow = this.regexPdfXml.test(filename);
    }
    else if (this.allowExtensions.pdfImage === true){
      allow = this.regexPdfImage.test(filename);
    }
    return allow;
  }
}

