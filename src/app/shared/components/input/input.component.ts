import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html'
})
export class InputComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  @Input() maxCharacters: number;

  ngOnInit(): void {

  }

}
