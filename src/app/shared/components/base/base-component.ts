import { EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

export class BaseComponent {

  @Input() name: string;
  @Input() cols = 'col-xs-12 col-sm-8 col-md-6 col-lg-4';
  @Input() label: string;
  @Input() formGroup: FormGroup;
  @Input() messageRequired = 'Campo obligatorio';
  @Input() disabled = false;

  @Output() onChange = new EventEmitter();
  @Output() onBlur = new EventEmitter();

  change(event) {
    this.onChange.emit(event.value);
  }

  blur(event){
    this.onBlur.emit(event.value);
  }

}
