import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';
import { RadioOptionI } from '../../models/radio-option.interface';

@Component({
  selector: 'app-checkbox',
  templateUrl: 'checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})

export class CheckboxComponent extends BaseComponent implements OnInit {

  @Input() checkboxOptions: RadioOptionI[] = [];

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
