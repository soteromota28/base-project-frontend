import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ThemePalette } from '@angular/material';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() type: string;
  @Input() color: ThemePalette;
  @Input() icon: string;
  @Input() text: string;
  @Input() routerLink: any[];
  @Input() toolTip: string;
  @Input() disabled: boolean;

  @Output() buttonClickEvent = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  buttonClick = (event: any) => {
    this.buttonClickEvent.emit(event);
  }


}
