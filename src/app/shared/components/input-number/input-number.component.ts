import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html'
})
export class InputNumberComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  @Input() maxCharacters: number;

  ngOnInit(): void {

  }

}
