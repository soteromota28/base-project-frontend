import { Component, Input, OnInit } from '@angular/core';
import { ProgressSpinnerMode, ThemePalette } from '@angular/material';

@Component({
  selector: 'app-progress-spinner',
  templateUrl: 'progress-spinner.component.html',
})

export class ProgressSpinnerComponent implements OnInit {

  @Input() color?: ThemePalette;
  @Input() diameter? = 100;
  @Input() mode?: ProgressSpinnerMode = 'indeterminate';
  @Input() strokeWidth?: number;
  @Input() value?: number;

  constructor() {
  }

  ngOnInit() {
  }
}
