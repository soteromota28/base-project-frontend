import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';
import { OptionI } from '../../models/option.interface';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html'
})
export class SelectComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  @Input() items: OptionI[] = [];

  ngOnInit(): void {
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }

}
