import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base-component';

@Component({
  selector: 'app-text-area',
  templateUrl: 'text-area.component.html',
})

export class TextAreaComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  @Input() maxCharacters: number;

  ngOnInit(): void {}
}
