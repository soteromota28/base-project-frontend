import { Injectable } from '@angular/core';


export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}


const MENU_GENERAL = [
  { state: 'dashboard', name: 'Dashboard', type: 'link', icon: 'home' },
];



@Injectable()
export class MenuItems {
  
  getMenuitemGeneral(): Menu[] {
    return MENU_GENERAL;
  }
}
